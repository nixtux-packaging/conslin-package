Summary: Консультант+ Conslin driver for Linux with systemd autostart
Name: conslin
Version: 0.1
Release: alt3
License: Proprietary
Group: System/Configuration/Packaging
Url: http://www.consultantkirov.ru/additional/useful/the-latest-version-of-the-files-consultant-plus/conslin.zip
Packager: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>
Source0: conslin.ia32.bin
Source1: conslin.service

BuildRequires: systemd
BuildRequires(pre): rpm-macros-fedora-compat
AutoReq: no
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%ifarch	ia86 x86 i386 i486 i586 i686
Requires: libstdc++6, libgcc1, glibc-core 
%endif

%ifarch x86_64
Requires: i586-libstdc++6, i586-libgcc1, i586-glibc-core 
%endif

# based on https://packages.altlinux.org/ru/Sisyphus/srpms/haspd/spec
# Пакет c этими 32-битными блобами все равно собирается 64-битным, а авторезолвер зависимостей не срабатывает.
# Ну и так соудет, т.к. там их нет, кроме гарантированно имеющихся в системе библиотек, см. ldd ./conslin.ia32.bin
ExclusiveArch: %ix86 x86_64

%description -l ru_RU.UTF-8
Драйвер conslin для Консультант+
и сервис systemd для его автозапуска

%prep
#%setup

%install
mkdir -p %buildroot%_bindir/
mkdir -p %buildroot%_unitdir/
install -p -m755 %{SOURCE0} %buildroot%_bindir/conslin
install -p -m 644 %{SOURCE1} %buildroot%_unitdir/conslin.service

# on ALT Linux systemd service will be disabled by default
# enable manually: systemctl enable conslin

%post
%systemd_post conslin.service

%preun
%systemd_preun conslin.service

%postun
%systemd_postun conslin.service

%files
%_bindir/conslin
%_unitdir/conslin.service

%changelog
* Sat Jul 29 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-alt3
- Fix 32 bit dependencies
* Sat Jul 29 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-alt1
- Initial packaging
